import pickle
import socket

from pymemcache.test.utils import MockMemcacheClient
from pymemcache.client.hash import HashClient


class MemcacheInterface:
    def __init__(self, client):
        self.client = client

    def get_cached_data(self, name: str):
        """
        :param name: Name of the key you wish to store the data in

        returns:
            :param result: data from the cache, else None
        """
        if not name:
            raise TypeError('Name must have a ascii value')

        result = self.client.get(name)

        if result:
            result = pickle.loads(result)

        return result

    def set_cached_data(self, data, name: str, expire_time=7200):
        """
        :param data: A python data type which can be serialised with Pickle
        :param name: Name of the key you wish to set the data in.

        optional:
        :param expire_time: the time to expire the data in seconds

        returns:
            :param success_flag: Boolean if the caching was successful
        """
        if not name:
            raise TypeError('Name must not be empty and have a ascii value')

        if expire_time <= 0:
            raise ValueError('cache expire_time must be greater than 0')

        data = pickle.dumps(data)

        if self.get_cached_data(name) is None:
            success_flag = self.client.set(name, data, expire=expire_time)
        else:
            success_flag = False

        return success_flag


class MemcacheClient(MemcacheInterface):
    def __init__(self, from_hostname):
        host = socket.gethostbyname(from_hostname)
        servers = [(host, 11211)]
        client = HashClient(servers, use_pooling=True)
        super().__init__(client)


class TestMemcacheClient(MemcacheInterface):
    def __init__(self):
        client = MockMemcacheClient()
        super().__init__(client)
